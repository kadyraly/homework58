import React, {Component, Fragment} from 'react';
import Modal from "./components/UI/Modal/Modal";
import Alert from "./components/UI/Alert/Alert";



class App extends Component {
   state = {
        show: false

   };

    modelOpen = () => {
        this.setState({show: true })
    };

    modelClose =() => {
        this.setState({show: false })
    };

    someHandler=() => {

    };

    render() {
        return (
            <Fragment>
                <Modal
                    show={this.state.show}
                    closed={this.modelClose}
                    title="Some kinda modal title"
                    clicked={this.someHandler}
                >
                    <p>This is modal content</p>

                </Modal>
                <button onClick={this.modelOpen}>open</button>
                <Alert
                    type="warning"
                    dismiss={this.someHandler}
                >This is a warning type alert</Alert>
                <Alert type="success">This is a success type alert</Alert>
                <Alert type="primary">This is a primary type alert</Alert>
                <Alert type="danger">This is a danger type alert</Alert>

            </Fragment>

        );
    }
}

export default App;
