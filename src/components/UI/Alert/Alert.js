import React from 'react';
import './Alert.css';

const Alert  = props => {

    return (

        <div className={['Alert', props.type].join(' ')} >
            {props.dismiss ? <button>X</button>: null}
            {props.children}

        </div>


    )
};

export default Alert;