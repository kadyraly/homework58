import React from 'react';
import './Modal.css';

import Backdrop from "../Backdrop/Backdrop";
import Wrapper from "../../../hoc/Wrapper";
import Button from "../Button/Button";


const Modal = props => {


return(
    <Wrapper>
        <Backdrop show={props.show} clicked={props.closed}/>
        <div
            className="Modal"
            style={{
                transform: props.show ? 'translate(0)' : 'translateY(-100vh)',
                opacity: props.show ? '1' : '0'
            }}
        >
            {props.children}
            <Button clicked={props.closed}>X</Button>
        </div>

    </Wrapper>
)


};

export default Modal;